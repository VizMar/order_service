package com.example.demo;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.orderservice.client.OrderClient;
import com.orderservice.entity.OrderData;
import com.orderservice.service.ManageOrderImpl;

@SpringBootTest
public class TestService {
	
	
	@InjectMocks
	ManageOrderImpl manageOrderImpl;
	
	@Mock
	OrderClient orderClient;
	
	
	
	
	@BeforeEach
	public void setUp() {
		//this.manageOrderImpl;
	}

	@Test
	public void testGetOrder() {
		
		//when(orderClient.getItem(Mockito.anyInt())).thenReturn();
		
		List<OrderData> result = manageOrderImpl.getOrders();
		
		assert(result != null);
	}
}
