package com.orderservice.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.orderservice.client.OrderClient;
import com.orderservice.dao.OrderRepository;
import com.orderservice.entity.OrderData;
import com.orderservice.entity.OrderedItem;
import com.orderservice.service.ManageOrderImpl;
import com.orderservice.service.exceptionHandler.ItemNotFoundException;
import com.orderservice.vo.OrderSummary;

@RestController
public class OrderServiceController {

	@Autowired
	ManageOrderImpl manageOrderImpl;

	@Autowired
	OrderClient orderClient;

	@Autowired
	OrderRepository repository;

	@PostMapping(value ="orderservice/create", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
	public OrderSummary createOrder(@Valid @RequestBody OrderData order) {
		OrderSummary orderSummary = new OrderSummary();
		orderSummary = manageOrderImpl.createOrder(order);
		return orderSummary;
	}
	
	@GetMapping("/orderservice/{orderId}")
	public ResponseEntity<OrderData> getOrderId(@PathVariable int orderId) {

		Optional<OrderData> order = repository.findById(orderId);
		return new ResponseEntity<OrderData>(order.get(),HttpStatus.OK);
		
	}
	

	@GetMapping("/test/{productCode}")
	public ResponseEntity<OrderedItem> testFeignClient(@PathVariable int productCode) {

		ResponseEntity<OrderedItem> item = orderClient.getItem(productCode);
		return item;
	}

}
