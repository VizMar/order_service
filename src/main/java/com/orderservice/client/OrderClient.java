
package com.orderservice.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.orderservice.entity.OrderedItem;

@Service

@FeignClient(value = "orderItem", url = "http://localhost:8081")
public interface OrderClient {

	@GetMapping("/items/{productCode}")
	public ResponseEntity<OrderedItem> getItem(@PathVariable int productCode);
}
