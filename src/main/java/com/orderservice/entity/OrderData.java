package com.orderservice.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "orderdata")
public class OrderData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Integer orderId;
	
	@NotBlank(message = "Customer name is mandatory")
	String customerName;

	@Embedded
	@ElementCollection
	List<OrderedItem> orderedItem;

	@Embedded
	ShipmentAddress shipmentAddress;
	
	Date orderedDate;
	
	double totalPrice;

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public List<OrderedItem> getOrderedItem() {
		return orderedItem;
	}

	public void setOrderedItem(List<OrderedItem> orderedItem) {
		this.orderedItem = orderedItem;
	}

	public ShipmentAddress getShipmentAddress() {
		return shipmentAddress;
	}

	public void setShipmentAddress(ShipmentAddress shipmentAddress) {
		this.shipmentAddress = shipmentAddress;
	}

}
