
package com.orderservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.orderservice.client.OrderClient;
import com.orderservice.dao.OrderRepository;
import com.orderservice.entity.OrderedItem;
import com.orderservice.entity.OrderData;
import com.orderservice.service.exceptionHandler.ItemNotFoundException;
import com.orderservice.vo.OrderSummary;
import com.orderservice.vo.OrderedServiceVo;

@Service
public class ManageOrderImpl implements ManageOrder {

	@Autowired
	OrderClient orderClient;

	@Autowired
	OrderRepository orderRepository;

	double totalPrice = 0;

	int itemCount = 0;

	@Override
	public OrderSummary createOrder(OrderData input) {

		OrderSummary orderSummary = new OrderSummary();

		UnaryOperator<OrderedItem> OrderTosave = (OrderedItem item) -> {
			OrderedItem response = new OrderedItem();

			try {
				response = callItemClient(item);
			} catch (ItemNotFoundException e) {
				e.getMessage();
			}

			return response;
		};

		List<OrderedItem> outputList = convertTofetchedType(input.getOrderedItem(), OrderTosave);

		orderSummary.setTotalPrice(totalPrice);
		orderSummary.setNumberOfItems(itemCount);
		totalPrice = 0;
		itemCount = 0;
		input.setOrderedItem(outputList);
		orderRepository.save(input);
		return orderSummary;
	}

	private List<OrderedItem> convertTofetchedType(List<OrderedItem> orderItems,
			UnaryOperator<OrderedItem> orderTosave) {

		List<OrderedItem> outputList = new ArrayList<OrderedItem>();
		for (OrderedItem object : orderItems) {
			outputList.add(orderTosave.apply(object));
		}
		return outputList;
	}

	private OrderedItem callItemClient(OrderedItem item) throws ItemNotFoundException {
		ResponseEntity<OrderedItem> result = null;
		int productCode = item.getProductCode();

		try {
			result = orderClient.getItem(productCode);
		}

		catch (RuntimeException ex) {

			throw new ItemNotFoundException("Not found");
		}

		calculatePrice(result, item.getQuantity());

		return result.getBody();
	}

	private void calculatePrice(ResponseEntity<OrderedItem> result, int quantity) {
		itemCount++;
		OrderedItem itemFetched = result.getBody();
		totalPrice = totalPrice + (itemFetched.getPrice() * quantity);
	}

	@Override
	public List<OrderData> getOrders() { // TODO Auto-generated method stub
	List<OrderData> data = new ArrayList<>();
	return data;
	}

	@Override
	public OrderData getOrder(int orderId) { // TODO Auto-generated method
		return null;
	}

}
