package com.orderservice.service;

import java.util.List;

import com.orderservice.entity.OrderData;
import com.orderservice.vo.OrderSummary;

public interface ManageOrder {
	
	public OrderSummary createOrder(OrderData input);
	
	public List<OrderData> getOrders();
	
	public OrderData getOrder(int orderId);

}
